<?php

/**
 * php update-threads out2/{fileName} {offset}
 */

require 'bootstrap.php';

use GuzzleHttp\Exception\ServerException;
use League\Csv\Statement;

$file = __DIR__ . "/{$argv[1]}";
$offset = (int) ($argv[2] ?? 0);
$limit = $params['api_limit'];

$reader = createCsvReader($file);
$stmt = (new Statement())
    ->offset($offset)
    ->limit($limit)
;
$records = $stmt->process($reader);
$rows = [];
$api = createApiClient();
$i = $offset;
foreach ($records as $record) {
    $link = $record[0];
    $identifier = $record[1];
    $thread = $record[2];
    try {
        $api->post(
            'threads/update.json',
            [
                'http_errors' => true,
                'query' => [
                    'api_key' => $params['api_key'],
                    'access_token' => $params['access_token'],
                    'thread' => $thread,
                    'identifier' => $identifier,
                ],
            ]
        );
    } catch (ServerException $e) {
        $rows[] = [
            $link,
            $identifier,
            $thread
        ];
    }
    $i++;
}
$writer = createCsvWriter(str_replace('out2', 'error', $file), 'a');
$writer->insertAll($rows);
echo $i;
