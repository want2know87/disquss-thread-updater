<?php

require 'bootstrap.php';

$filter = function ($file) {
    return !(strpos($file, '.') === 0);
};
$files = array_filter(
    scandir(__DIR__ . '/in'),
    $filter
);
foreach ($files as $file) {
    $reader = createCsvReader(__DIR__ . "/in/$file");
    $rows = [];
    $i = 0;
    foreach ($reader->getRecords() as $record) {
        // if ($i === 5) {
        //     break;
        // }
        if (matchMask($record[0])) {
            $rows[] = [
                $record[0],
                createIdentifier($record[0]),
            ];
            $i++;
        }
    }
    $writer = createCsvWriter(__DIR__ . "/out/$file");
    $writer->insertAll($rows);
}
