<?php

use GuzzleHttp\Client;
use League\Csv\Reader;
use League\Csv\Writer;

/**
 * @param string $path
 * @return Reader
 */
function createCsvReader($path, $mode = 'r')
{
    return Reader::createFromPath($path, $mode);
}

/**
 * @param string $path
 * @return Writer
 */
function createCsvWriter($path, $mode = 'w')
{
    return Writer::createFromPath($path, $mode);
}

/**
 * @return string
 */
function domainMask()
{
    return '/^https:\/\/(ru|en)\.ostrog\.com(\/m)?\/(.+)\/(\?.+)?/';
}

/**
 * @param string $url
 * @return boolean
 */
function matchMask($url)
{
    return preg_match(domainMask(), $url) === 1;
}

/**
 * @param string $url
 * @return string
 */
function createIdentifier($url)
{
    return preg_replace(domainMask(), '$1-thread---$3', $url);
}

/**
 * @param string $path
 * @return boolean
 */
function isCsvFile($path)
{
    return preg_match('/\.csv$/', $path) === 1;
}

/**
 * @param string $dir
 * @return array
*/
function scanDirForCsvFiles($dir)
{
    return array_filter(scandir($dir), 'isCsvFile');
}

// api

/**
 * @return Client
 */
function createApiClient()
{
    return new Client([
        'base_uri' => 'https://disqus.com/api/3.0/',
    ]);
}

function getThreadId($response)
{
    $result = json_decode($response->getBody()->getContents(), true);
    return $result['response']['id'] ?? 'NOT FOUND';
}
