<?php

/**
 * php write-thread-ids out/{fileName} {offset}
 */

require 'bootstrap.php';

use League\Csv\Statement;

$file = __DIR__ . "/{$argv[1]}";
$offset = (int) ($argv[2] ?? 0);
$limit = $params['api_limit'];

$reader = createCsvReader($file);
$stmt = (new Statement())
    ->offset($offset)
    ->limit($limit)
;
$records = $stmt->process($reader);
$rows = [];
$api = createApiClient();
foreach ($records as $record) {
    $response = $api->get(
        'threads/details.json',
        [
            'query' => [
                'api_key' => $params['api_key'],
                'thread:link' => $record[0],
                'forum' => 'enstrog',
            ],
        ]
    );
    $thread = getThreadId($response);
    $rows[] = [
        $record[0],
        $record[1],
        $thread
    ];
}
$writer = createCsvWriter(str_replace('out', 'out2', $file), 'a');
$writer->insertAll($rows);
